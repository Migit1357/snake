/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*------------------------------------------------------------
 *Variables
 -------------------------------------------------------------*/
var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var Highscore = localStorage.getItem(HighscoreX);
var HighscoreX;

var food;

var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;

var startGameMenu;
var playButton;

var levelSelctorMenu;
var easy;
var medium;
var hard;
var foodImage;
var snakeImage;
var sound;
var growSound;
var ggSound;
/*
 * functions
 */

gameInitalize();
snakeInitalize();
foodInitialize();
gameDraw();
sound.play();





/*------------------------------------------------------------
 *First function that allows the game to work
 -------------------------------------------------------------*/

function gameInitalize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    canvas.width = screenWidth;
    canvas.height = screenHeight;
    
    foodImage = document.getElementById("FoodImage");
    
    snakeImage = document.getElementById("SnakeImage");

    document.addEventListener("keydown", keyboardHandler);

    startGameMenu = document.getElementById("startGame");
    centerMenuPosition(startGameMenu);

    playButton = document.getElementById("playButton");
    playButton.addEventListener("click", gameBegin);

    levelSelectorMenu = document.getElementById("levelSelect");
    centerMenuPosition(levelSelectorMenu);

    easy = document.getElementById("easyButton");
    easy.addEventListener("click", gameEasy);
    medium = document.getElementById("mediumButton");
    medium.addEventListener("click", gameMedium);
    hard = document.getElementById("hardButton");
    hard.addEventListener("click", gameHard);

    Highscore = document.getElementById("Highscore");

    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);

    restartButton = document.getElementById("restart");
    restartButton.addEventListener("click", gameRestart);

    playHUD = document.getElementById("playHUD");

    scoreboard = document.getElementById("scoreboard");
    
    sound = new Audio("arcade.mp3");
    sound.preload = "auto";
    
    growSound = new Audio("error.wav");
    growSound.preload = "auto";
    
    ggSound = new Audio("laugh.wav");
    ggSound.preload = "auto";

    setState("START");
    
}
/*------------------------------------------------------------
 *Makes these codes repeat 
 -------------------------------------------------------------*/
function gameLoop() {
    if (gameState === "START") {
        gameInitalize();
        gameDraw();

    }
    
    gameDraw();
    if (gameState === "PLAY EASY") {
        
        snakeDraw();
        foodDraw();
        snakeUpdate();
        drawScoreboard();
        HighScore();
        
        
    }
    if (gameState === "PLAY MEDIUM") {
        
        snakeDraw();
        foodDraw();
        snakeUpdate();
        drawScoreboard();
        HighScore();
       
    }
    if (gameState === "PLAY HARD") {
        
        snakeDraw();
        foodDraw();
        snakeUpdate();
        drawScoreboard();
        HighScore();
       
    }
    if (gameState === "SELECTOR") {
        gameDraw();
        Highscore.style.visibility = "hidden";
        sound.play();


    }
    if (gameState === "GAME OVER") {
        sound.pausel();
    }
}
/*------------------------------------------------------------
 *makes the background one color
 -------------------------------------------------------------*/
function gameDraw() {
    context.fillStyle = "black";
    context.fillRect(0, 0, screenWidth, screenHeight);

}
function gameBegin() {
    snakeInitalize();
    foodInitialize();
    startGameMenu.style.visibility = "hidden";
    setState("SELECTOR");

}

function  gameEasy() {
    
    snakeInitalize();
    foodInitialize();
    setState("PLAY EASY");
    Speed = 1200 / 30;
    setInterval(gameLoop, 1300 / 30);
    scoreboard.style.visibility = "visible";
    Highscore.style.visibility = "visible";
    levelSelectorMenu.style.visibility = "Hidden";
}

function  gameMedium() {
    
    snakeInitalize();
    foodInitialize();
    setState("PLAY MEDIUM");
    Speed = 1000 / 30;
    setInterval(gameLoop, 1100 / 30);
    Highscore.style.visibility = "visible";
    scoreboard.style.visibility = "visible";
    levelSelectorMenu.style.visibility = "Hidden";

}

function  gameHard() {
    snakeInitalize();
    foodInitialize();
    setState("PLAY HARD");
    Speed = 1100 / 50;
    setInterval(gameLoop, 1200 / 40);
    Highscore.style.visibility = "visible";
    scoreboard.style.visibility = "visible";
    levelSelectorMenu.style.visibility = "Hidden";

}

function gameRestart() {
    gameOverMenu.style.visibility = "hidden";
    setState("SELECTOR");
    scoreboard.style.visibility = "hidden";
    gameLoop = gameLoop - 1;
    
    
}
/*------------------------------------------------------------
 *this makes the snake
 -------------------------------------------------------------*/
function snakeInitalize() {
    snake = [];
    snakeLength = 5;
    snakeSize = 30;
    snakeDirection = "down";

    for (var index = snakeLength - 1; index >= 0; index--) {
        snake.push({
            x: 10,
            y: 10
        });
    }

}
/*------------------------------------------------------------
 * this draws the snake
 -------------------------------------------------------------*/
function snakeDraw() {
    for (var index = 0; index < snake.length; index++) {
        context.drawImage(snakeImage, snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        /*
        context.strokeStyle = "rgb(0, 255, 255)";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth = 4;
        */
    }
}
/*------------------------------------------------------------
 * this allows the sanke to move
 -------------------------------------------------------------*/
function snakeUpdate() {
    var snakeHeadX = snake [0].x;
    var snakeHeadY = snake [0].y;

    if (snakeDirection == "down") {
        snakeHeadY++;
    }
    else if (snakeDirection == "right") {
        snakeHeadX++;
    }
    else if (snakeDirection == "left") {
        snakeHeadX--;
    }
    else if (snakeDirection == "up") {
        snakeHeadY--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);

    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
}
/*------------------------------------------------------------
 *creates the food
 -------------------------------------------------------------*/
function foodInitialize() {
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}
/*------------------------------------------------------------
 * draws the food on the website
 -------------------------------------------------------------*/
function foodDraw() {
    context.drawImage(foodImage, food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
}

/*****************************************************************
 * This will put the food in a random position on the screen
 ******************************************************************* */

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * (screenWidth));
    var randomY = Math.floor(Math.random() * (screenHeight));

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}
/*_________________________________________________________________________________
 * input functions
 __________________________________________________________________________________*/

function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection !== "left") {
        snakeDirection = "right";
    }
    else if (event.keyCode == "40" && snakeDirection !== "up") {
        snakeDirection = "down";
    }
    else if (event.keyCode == "38" && snakeDirection !== "down") {
        snakeDirection = "up";
    }
    else if (event.keyCode == "37" && snakeDirection !== "right") {
        snakeDirection = "left";
    }
}
/*____________________________________________________________________________-
 * Collisions
 ______________________________________________________________________________*/

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y) {
        snake.push({
            x: 0,
            y: 0
        });
        snake.push({
            x: 0,
            y: 0
        });
        snake.push({
            x: 0,
            y: 0
        });
        snakeLength++;
        snakeLength++;
        snakeLength++;
        
        
        foodDraw();
        setFoodPosition();
        growSound.play();
    }
}
function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        ggSound.play();
        setState("GAME OVER");
    }
    else if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        ggSound.play();
        setState("GAME OVER");
    }
}
function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for (var index = 1; index < snake.length; index++) {
        if (snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            
            ggSound.play();
            setState("GAME OVER");
            return;
        }
    }
}

/*=================================================================================
 * Game state
 ==============================================================================*/

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*_______________________________________________________________________________
 * Menu Functions
 _______________________________________________________________________________*/

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function showMenu(state) {
    if (state == "GAME OVER") {
        displayMenu(gameOverMenu);
    }
    else if (state == "PLAY EASY") {
        displayMenu(playHUD);
    }
    else if (state == "PLAY MEDIUM") {
        displayMenu(playHUD);
    }
    else if (state == "PLAY HARD") {
        displayMenu(playHUD);
    }
    else if (state == "START") {
        displayMenu(startGameMenu);
    }
    else if (state == "SELECTOR") {
        displayMenu(levelSelectorMenu);
    }
}

function centerMenuPosition(menu) {
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}


function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
    Highscore.innerHTML = "HighScore: " + localStorage.getItem(HighscoreX);
}

function HighScore() {
    if(localStorage.getItem(HighscoreX) < snakeLength) {
       localStorage.setItem(HighscoreX , snakeLength);
    }
    
}